package algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import algorithm.criteria.ClosestCriteria;
import algorithm.criteria.Criteria;
import algorithm.criteria.CriteriaOperations;
import algorithm.criteria.FurthestCriteria;
import algorithm.sort.AscendingSortByBirthday;

public class PeopleFinder {
    private final List<Person> people;
    private CriteriaOperations criteriaOperations;

    public PeopleFinder(List<Person> person) {
        criteriaOperations = null;
        people = person;
    }

    public PersonPair findPersonPairByCriteria(Criteria criteria) {
        PersonPair personPair = new PersonPair();
        if (people != null && !people.isEmpty()) {
            Collections.sort(people, new AscendingSortByBirthday());
            if (criteria.equals(Criteria.FURTHEST)) {
                criteriaOperations = new FurthestCriteria();
            } else if (criteria.equals(Criteria.CLOSEST)) {
                criteriaOperations = new ClosestCriteria();
            }
            personPair = criteriaOperations.findByPersonDistance(people);
        }
        return personPair;
    }
}
