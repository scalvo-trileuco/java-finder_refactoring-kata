package algorithm;

public class PersonPair {
    private Person youngPerson;
    private Person oldPerson;
    private long birthDateDistance;

    public PersonPair(Person person1, Person person2) {
        super();
        if (person1.getBirthDate()
                .getTime()
                - person2.getBirthDate()
                        .getTime() >= 0) {
            this.youngPerson = person2;
            this.oldPerson = person1;

        } else {
            this.youngPerson = person1;
            this.oldPerson = person2;
        }
        calculateBirthDateDistance();
    }

    public PersonPair() {

    }

    private void calculateBirthDateDistance() {
        if (this.youngPerson != null && this.oldPerson != null) {
            this.birthDateDistance = oldPerson.getBirthDate()
                    .getTime()
                    - youngPerson.getBirthDate()
                            .getTime();
        }
    }

    public long getBirthDateDistance() {
        return birthDateDistance;
    }

    public Person getYoungPerson() {
        return youngPerson;
    }

    public Person getOldPerson() {
        return oldPerson;
    }

}
