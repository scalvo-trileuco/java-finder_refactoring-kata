package algorithm.criteria;

import algorithm.PersonPair;

import java.util.List;

import algorithm.*;

public interface CriteriaOperations {
    public PersonPair findByPersonDistance(List<Person> personList);
}