package algorithm.criteria;

import algorithm.PersonPair;

public enum Criteria {
    CLOSEST, FURTHEST
}
