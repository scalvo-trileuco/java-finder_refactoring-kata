package algorithm.criteria;

import java.util.List;

import algorithm.Person;
import algorithm.PersonPair;

public class FurthestCriteria implements CriteriaOperations {

    @Override
    public PersonPair findByPersonDistance(List<Person> personList) {
        if (!personList.isEmpty() && personList.size() > 1) {
            return new PersonPair(personList.get(0), personList.get(personList.size() - 1));
        }
        return new PersonPair();
    }

}