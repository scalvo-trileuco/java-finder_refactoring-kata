package algorithm.criteria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import algorithm.Person;
import algorithm.PersonPair;
import algorithm.sort.LowestFirstSort;

public class ClosestCriteria implements CriteriaOperations {

    private Comparator<PersonPair> comparison;

    public ClosestCriteria() {
        comparison = new LowestFirstSort();
    }

    @Override
    public PersonPair findByPersonDistance(List<Person> personList) {
        List<PersonPair> peopleDistances = new ArrayList<PersonPair>();

        if (!personList.isEmpty() && personList.size() > 1) {
            for (int i = 0; i < personList.size() - 1; i++) {
                peopleDistances.add(new PersonPair(personList.get(i), personList.get(i + 1)));
            }
            if (!peopleDistances.isEmpty()) {
                Collections.sort(peopleDistances, comparison);
                return peopleDistances.get(0);
            }
        }
        return new PersonPair();
    }

}