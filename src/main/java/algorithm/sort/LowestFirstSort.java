package algorithm.sort;

import java.util.Comparator;

import algorithm.Person;
import algorithm.PersonPair;

public class LowestFirstSort implements Comparator<PersonPair> {

    @Override
    public int compare(PersonPair p1, PersonPair p2) {
        return p1.getBirthDateDistance() < p2.getBirthDateDistance() ? -1
                : (p1.getBirthDateDistance() == p2.getBirthDateDistance()) ? 0 : 1;
    }

}