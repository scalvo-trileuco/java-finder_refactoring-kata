package algorithm.sort;

import java.util.Comparator;

import algorithm.Person;

public class AscendingSortByBirthday implements Comparator<Person> {

    @Override
    public int compare(Person p1, Person p2) {
        return p1.getBirthDate().compareTo(p2.getBirthDate());
    }

}