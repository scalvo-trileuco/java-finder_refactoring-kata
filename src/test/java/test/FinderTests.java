package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import algorithm.criteria.Criteria;
import algorithm.PeopleFinder;
import algorithm.Person;
import algorithm.PersonPair;

public class FinderTests {

    Person sue = new Person();
    Person greg = new Person();
    Person sarah = new Person();
    Person mike = new Person();

    @Before
    public void setup() {
        sue.setName("Sue");
        sue.setBirthDate(new Date(50, 0, 1));
        greg.setName("Greg");
        greg.setBirthDate(new Date(52, 5, 1));
        sarah.setName("Sarah");
        sarah.setBirthDate(new Date(82, 0, 1));
        mike.setName("Mike");
        mike.setBirthDate(new Date(79, 0, 1));
    }

    @Test
    public void Returns_Empty_Results_When_Given_Empty_List() {
        List<Person> list = new ArrayList<Person>();
        PeopleFinder finder = new PeopleFinder(list);

        PersonPair result = finder.findPersonPairByCriteria(Criteria.CLOSEST);
        assertEquals(null, result.getYoungPerson());
        assertEquals(null, result.getOldPerson());
    }

    @Test
    public void Returns_Empty_Results_When_Given_One_Person() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);

        PeopleFinder finder = new PeopleFinder(list);

        PersonPair result = finder.findPersonPairByCriteria(Criteria.CLOSEST);

        assertEquals(null, result.getYoungPerson());
        assertEquals(null, result.getOldPerson());
    }

    @Test
    public void Returns_Closest_Two_For_Two_People() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(greg);
        PeopleFinder finder = new PeopleFinder(list);

        PersonPair result = finder.findPersonPairByCriteria(Criteria.CLOSEST);

        assertEquals(sue, result.getYoungPerson());
        assertEquals(greg, result.getOldPerson());
    }

    @Test
    public void Returns_Furthest_Two_For_Two_People() {
        List<Person> list = new ArrayList<Person>();
        list.add(mike);
        list.add(greg);

        PeopleFinder finder = new PeopleFinder(list);

        PersonPair result = finder.findPersonPairByCriteria(Criteria.FURTHEST);

        assertEquals(greg, result.getYoungPerson());
        assertEquals(mike, result.getOldPerson());
    }

    @Test
    public void Returns_Furthest_Two_For_Four_People() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(sarah);
        list.add(mike);
        list.add(greg);
        PeopleFinder finder = new PeopleFinder(list);

        PersonPair result = finder.findPersonPairByCriteria(Criteria.FURTHEST);

        assertEquals(sue, result.getYoungPerson());
        assertEquals(sarah, result.getOldPerson());
    }

    @Test
    public void Returns_Closest_Two_For_Four_People() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(sarah);
        list.add(mike);
        list.add(greg);

        PeopleFinder finder = new PeopleFinder(list);

        PersonPair result = finder.findPersonPairByCriteria(Criteria.CLOSEST);

        assertEquals(sue, result.getYoungPerson());
        assertEquals(greg, result.getOldPerson());
    }

}